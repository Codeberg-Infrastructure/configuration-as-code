inventory_staging := "environments/staging/inventory.ini"
inventory_prod := "environments/prod/inventory.ini"
user := env_var_or_default("SSH_USER", "$(whoami)")
playbook_ci_server := "playbooks/ci/server.yml"
playbook_ci_agent := "playbooks/ci/agent.yml"
playbook_pages := "playbooks/pages/run.yml"
playbook_valkey := "playbooks/valkey/run.yml"
playbook_configure_host := "playbooks/configure/host.yaml"
playbook_configure_docker := "playbooks/configure/docker.yaml"

lint:
  ansible-lint

### Configure host

configure_host_production_apply:
  ansible-playbook -D -i {{ inventory_prod }} -bu {{ user }} {{ playbook_configure_host }}

configure_host_production_check:
  ansible-playbook -C -D -i {{ inventory_prod }} -bu {{ user }} {{ playbook_configure_host }}

configure_host_staging_apply:
  ansible-playbook -D -i {{ inventory_staging }} -bu {{ user }} {{ playbook_configure_host }}

configure_host_staging_check:
  ansible-playbook -C -D -i {{ inventory_staging }} -bu {{ user }} {{ playbook_configure_host }}

### Configure docker

configure_docker_production_apply:
  ansible-playbook -D -i {{ inventory_prod }} -bu {{ user }} {{ playbook_configure_docker }}

configure_docker_production_check:
  ansible-playbook -C -D -i {{ inventory_prod }} -bu {{ user }} {{ playbook_configure_docker }}

configure_docker_staging_apply:
  ansible-playbook -D -i {{ inventory_staging }} -bu {{ user }} {{ playbook_configure_docker }}

configure_docker_staging_check:
  ansible-playbook -C -D -i {{ inventory_staging }} -bu {{ user }} {{ playbook_configure_docker }}

### CI SERVER
ci_server_staging_check:
  ansible-playbook -C -D -i {{ inventory_staging }} -l ci_staging -bu {{ user }} {{ playbook_ci_server }} --extra-vars "woodpecker_forgejo_client_id=$WOODPECKER_FORGEJO_CLIENT_ID_STAGING woodpecker_forgejo_client_secret=$WOODPECKER_FORGEJO_CLIENT_SECRET_STAGING cibot_webhook_secret=$CIBOT_WEBHOOK_SECRET cibot_authorization_header=$CIBOT_AUTHORIZATION_HEADER cibot_token=$CIBOT_TOKEN cibot_team_id=$CIBOT_TEAM_ID"

ci_server_prod_check:
  ansible-playbook -C -D -i {{ inventory_prod }} -l ci_server_prod -bu {{ user }} {{ playbook_ci_server }} --extra-vars "woodpecker_forgejo_client_id=$WOODPECKER_FORGEJO_CLIENT_ID woodpecker_forgejo_client_secret=$WOODPECKER_FORGEJO_CLIENT_SECRET cibot_webhook_secret=$CIBOT_WEBHOOK_SECRET cibot_authorization_header=$CIBOT_AUTHORIZATION_HEADER cibot_token=$CIBOT_TOKEN cibot_team_id=$CIBOT_TEAM_ID"

ci_server_staging_apply:
  ansible-playbook -D -i {{ inventory_staging }} -l ci_staging -bu {{ user }} {{ playbook_ci_server }} --extra-vars "woodpecker_forgejo_client_id=$WOODPECKER_FORGEJO_CLIENT_ID_STAGING woodpecker_forgejo_client_secret=$WOODPECKER_FORGEJO_CLIENT_SECRET_STAGING cibot_webhook_secret=$CIBOT_WEBHOOK_SECRET cibot_authorization_header=$CIBOT_AUTHORIZATION_HEADER cibot_token=$CIBOT_TOKEN cibot_team_id=$CIBOT_TEAM_ID"

ci_server_prod_apply:
  ansible-playbook -D -i {{ inventory_prod }} -l ci_server_prod -bu {{ user }} {{ playbook_ci_server }} --extra-vars "woodpecker_forgejo_client_id=$WOODPECKER_FORGEJO_CLIENT_ID woodpecker_forgejo_client_secret=$WOODPECKER_FORGEJO_CLIENT_SECRET cibot_webhook_secret=$CIBOT_WEBHOOK_SECRET cibot_authorization_header=$CIBOT_AUTHORIZATION_HEADER cibot_token=$CIBOT_TOKEN cibot_team_id=$CIBOT_TEAM_ID"

### CI AGENT
ci_agent_staging_check:
  ansible-playbook -C -D -i {{ inventory_staging }} -l ci_staging -bu {{ user }} {{ playbook_ci_agent }} --extra-vars "woodpecker_agent_secret=$WOODPECKER_AGENT_SECRET_STAGING woodpecker_agent_secret_baarkerlounger=$WOODPECKER_AGENT_SECRET_BAARKERLOUNGER_STAGING"

ci_agent_prod_check:
  ansible-playbook -C -D -i {{ inventory_prod }} -l ci_agent_prod -bu {{ user }} {{ playbook_ci_agent }} --extra-vars "woodpecker_agent_secret=$WOODPECKER_AGENT_SECRET woodpecker_agent_secret_baarkerlounger=$WOODPECKER_AGENT_SECRET_BAARKERLOUNGER"

ci_agent_staging_apply:
  ansible-playbook -D -i {{ inventory_staging }} -l ci_staging -bu {{ user }} {{ playbook_ci_agent }} --extra-vars "woodpecker_agent_secret=$WOODPECKER_AGENT_SECRET_STAGING woodpecker_agent_secret_baarkerlounger=$WOODPECKER_AGENT_SECRET_BAARKERLOUNGER_STAGING"

ci_agent_prod_apply:
  ansible-playbook -D -i {{ inventory_prod }} -l ci_agent_prod -bu {{ user }} {{ playbook_ci_agent }} --extra-vars "woodpecker_agent_secret=$WOODPECKER_AGENT_SECRET woodpecker_agent_secret_baarkerlounger=$WOODPECKER_AGENT_SECRET_BAARKERLOUNGER"

### Pages
pages_staging_check:
  ansible-playbook -C -D -i {{ inventory_staging }} -l pages_staging -bu {{ user }} {{ playbook_pages }} --extra-vars "gitea_api_key=$GITEA_API_KEY gandi_api_key=$GANDI_API_KEY acme_email=$ACME_EMAIL"

pages_prod_check:
  ansible-playbook -C -D -i {{ inventory_prod }} -l pages_prod -bu {{ user }} {{ playbook_pages }} --extra-vars "gitea_api_key=$GITEA_API_KEY gandi_api_key=$GANDI_API_KEY acme_email=$ACME_EMAIL"

pages_staging_apply:
  ansible-playbook -D -i {{ inventory_staging }} -l pages_staging -bu {{ user }} {{ playbook_pages }} --extra-vars "gitea_api_key=$GITEA_API_KEY gandi_api_key=$GANDI_API_KEY acme_email=$ACME_EMAIL"

pages_prod_apply:
  ansible-playbook -D -i {{ inventory_prod }} -l pages_prod -bu {{ user }} {{ playbook_pages }} --extra-vars "gitea_api_key=$GITEA_API_KEY gandi_api_key=$GANDI_API_KEY acme_email=$ACME_EMAIL"

### Valkey
valkey_staging_check:
  ansible-playbook -C -D -i {{ inventory_staging }} -l ci_staging -bu {{ user }} {{ playbook_valkey }} --extra-vars "valkey_password=$VALKEY_PASSWORD_STAGING"

valkey_prod_check:
  ansible-playbook -C -D -i {{ inventory_prod }} -l ci_agent_prod -bu {{ user }} {{ playbook_valkey }} --extra-vars "valkey_password=$VALKEY_PASSWORD_PROD"

valkey_staging_apply:
  ansible-playbook -D -i {{ inventory_staging }} -l ci_staging -bu {{ user }} {{ playbook_valkey }} --extra-vars "valkey_password=$VALKEY_PASSWORD_STAGING"

valkey_prod_apply:
  ansible-playbook -D -i {{ inventory_prod }} -l ci_agent_prod -bu {{ user }} {{ playbook_valkey }} --extra-vars "valkey_password=$VALKEY_PASSWORD_PROD"
