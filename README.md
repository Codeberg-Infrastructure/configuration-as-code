---
gitea: none
include_toc: true
---

# Codeberg - Configuration

Codeberg's Configuration (as Code, using Ansible).
Parallel efforts are happening in [scripted-configuration](https://codeberg.org/Codeberg-Infrastructure/scripted-configuration).

Additional information can be found in the [Wiki](https://codeberg.org/Codeberg-Infrastructure/configuration-as-code/wiki).

## Applying Changes

### Local Apply

- Requires a hardware-backed SSH key (e.g. Yubikey) and assumes the username on Codeberg server to be the same as on the local machine
- Requires secrets to be locally available as env vars.
  By default empty defaults will be used to mimic the execution of the ansible modules in the playbook.
  Before executing one of the Justfile `prod` rules, export the environment variables listed in the respective `group_vars` files with the correct values.
  The values are stored in the secret management system or are shown when running the check workflow with the `-D` (diff) flag.
  A handy tool for storing local env vars is [`direnv`](https://direnv.net/).

Justfile rules are available in `Justfile`.

Inventory: Since the apply happens on a local machine, the LXC hostnames cannot be resolved.
A jumphost connection to "kampenwand" is required to reach the LXC containers.

### Remote Apply

The workflow approach is as follows:

1. PRs do `check` runs on the `staging` & `prod` env
2. Run on `main` do `apply` runs for the `staging` env and checks on `prod`
3. To deploy to `prod`, one needs to manually trigger the "Deploy" button in the WP UI after a **successful** execution of all steps.

## Secrets

Are stored in Woodpecker.
Secrets will only be injected for run on `main`.
On PRs, only "check" runs are executed using empty variable values for all secrets.

A neat hidden feature of using Woodpecker in combination with Ansible is that if secrets are specified as env vars, they will be automatically masked in the logs.

Because Woodpecker does not support RBAC for secret handling, the long-term goal is to move to a centralized, RBAC-enabled secret management system.

To execute the Justfile rules of this repo, the respective secrets specified in `--extra-vars` must be present locally as env vars.
In addition, SSH access is required.

## Ansible Dependencies

Are listed in `requirements.yml` and can be installed with `ansible-galaxy install -r requirements.yml`.

We recommend to keep the ansible-core version up-to-date to avoid issues with the required dependencies.

## User Access

User access is defined in the respective `host_vars` files in the `authorized_users` list.
The user SSH keys live in `collections/ansible_collections/codeberg/configure/roles/host/files/pubkeys/`
The group `codeberg-sudoers` permits root access.
To add new users, add a _valid UNIX username_ to the list including a `-sk` (FIDO2) SSH public key.

## Environments

### prod

Host: `Kampenwand`

LXC containers:

| Name             | OS     | LXC                |
| :--------------- | :----- | :----------------- |
| `ci_server_prod` | Debian | ci.lxc.local       |
| `ci_agent_prod`  | Debian | ci-agent.lxc.local |
| `pages_prod`     | Debian | pages.lxc.local    |

### Staging

Host: `Netcup`

LXC containers:

| Name            | OS     | LXC                     |
| :-------------- | :----- | :---------------------- |
| `ci_staging`    | Alma9  | ci-staging.lxc.local    |
| `pages_staging` | Debian | pages-staging.lxc.local |

## Roles

Are organized in a collection under `collection/`.

### core.pages_server

Deploys the pages server.
Runs on each push to `main`

### core.valkey

Deploys [Valkey](https://valkey.io/) via Docker.

### configure.docker

Installs Docker on the host.
Runs on each push to `main` and on schedule (once per week)

### configure.host

Applies the basic configuration to the host.
This includes setting up users and groups, granting root access and defining `sshd` options.
Runs on each push to `main` and on schedule (once per week)

### ci.agent

Deploys the CI agent.
Runs on each push to `main`.

IPV6 connectivity:

- Set the following settings in `daemon.json`:
  ```json
  {
    "experimental": true,
    "ip6tables": true,
    "ipv6": true,
    "fixed-cidr-v6": "<ipv6 interface>"
  }
  ```
- Set `WOODPECKER_BACKEND_DOCKER_ENABLE_IPV6: true`
- (optional) Set `WOODPECKER_BACKEND_DOCKER_NETWORK: "<ipv6 enabled docker network>"` and create an ipv6-enabled docker network before

### ci.server

Deploys the CI server and cibot.
Runs on each push to `main`.

#### cibot

The [cibot] is deployed as a docker container with the `ci/server` role into the `ci_server_prod` LXC container.
It requires two secrets (`cibot_token`, `cibot_team_id`) to function and an additional
two (`cibot_webhook_secret`, `cibot_authorization_header`) can be specified to protect
the publicly exposed webhook endpoint against malicious behaviour.

It is exposed via [HAProxy] and configured via [config.scfg].

[cibot]: https://codeberg.org/Codeberg-CI/cibot
[HAProxy]: https://codeberg.org/Codeberg-Infrastructure/scripted-configuration/commit/4b686f9b880a59af05c9110c18602e9ef6de8cc6
[config.scfg]: https://codeberg.org/Codeberg-Infrastructure/ansible-configuration/src/branch/main/collections/ansible_collections/codeberg/ci/roles/server/templates/config.scfg.j2
